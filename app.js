const fs = require('fs');
const path = require('path');
const express = require('express');
const cors = require('cors');
const swaggerUi = require('swagger-ui-express');
const swaggerDocument = require('./swagger.json');

const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

/** Define a path to log activity */ 
const logFilePath = path.join(__dirname, 'logs',  'activity.log');

/** 
 * Write in a log file
 * */ 
const logToFile = (message) => {
    const logMessage = `${new Date().toISOString()} - ${message}\n`;

    fs.mkdir(path.dirname(logFilePath), { recursive: true }, (err) => {
        if (err) {
            return console.error('Error creating log directory:', err);
        }

        fs.appendFile(logFilePath, logMessage, (err) => {
            if (err) {
                console.error('Error writing to log file:', err);
            }
        });
    });
};



/** 
 * Middleware to check if the token is valid
*/
const activityLogger = (req, res, next) => {
    if (req.path.startsWith('/api-docs')) {
        return next();
    }

    const token = req.headers['x-student-token'];
    if (!token) {
        // Si aucun token n'est fourni, vous pourriez choisir de ne pas logger ou de logger différemment
        console.log('No token provided');
        logToFile('No token provided, access denied');
        return res.status(401).json({ message: 'Token is required' });
    } else {
        // Construire le message de log avec le token et les infos de la requête
        const message = `${new Date().toISOString()} - Token: ${token}, Method: ${req.method}, Path: ${req.path}`;
        // logActivity(message);
        logToFile(message);
    }
    next();
};


app.use(activityLogger);

// Static data for the API
let stations = [
    {
        id: 2654984,
        name: 'Station La Gardiole',
        location: 'Gigean',
        sensors: [
            {
                id: 1359841,
                type: 'Temperature',
                unit: 'Celsius',
                data: [
                    { id: 1, value: 24, datetime: 6456464465464 },
                    { id: 9959, value: -10, datetime: 1708078860717 },
                    { id: 5870, value: 29, datetime: 1708078860717 },
                    { id: 4445, value: -2, datetime: 1708078860717 },
                    { id: 3604, value: -2, datetime: 1708078860717 }
                ]
            }
        ]
    },
    {
        id: 2446544,
        name: 'Station Grand Som',
        location: 'Saint Pierre de chartreuse',
        sensors: [
            {
                id: 895134,
                type: 'Vent',
                unit: 'km/h',
                data: [
                    { id: 2444, value: 77, datetime: 1708078860717 },
                    { id: 4907, value: 79, datetime: 1708078860717 },
                    { id: 2040, value: 48, datetime: 1708078860717 },
                    { id: 8814, value: 127, datetime: 1708078860717 },
                    { id: 2571, value: 116, datetime: 1708078860717 }
                ]
            },
            {
                id: 895134,
                type: 'Temperature',
                unit: 'Celsius',
                data: [
                    { id: 1599, value: 26, datetime: 1708078860717 },
                    { id: 4951, value: 15, datetime: 1708078860717 },
                    { id: 6269, value: -8, datetime: 1708078860717 },
                    { id: 3754, value: 0, datetime: 1708078860717 },
                    { id: 2221, value: 40, datetime: 1708078860717 }
                ]
            }
        ]
    },
    {
        id: 8413115,
        name: 'Station Bois Barbue',
        location: 'Villard de lans',
        sensors: [
            {
                id: 98469355,
                type: 'Snow Depth',
                unit: 'cm',
                data: [
                    { id: 3212, value: 180, datetime: 1708078860717 },
                    { id: 3679, value: 118, datetime: 1708078860717 },
                    { id: 8176, value: 94, datetime: 1708078860717 },
                    { id: 6883, value: 32, datetime: 1708078860717 },
                    { id: 4730, value: 189, datetime: 1708078860717 }
                ]
            }
        ]
    },
    {
        id: 2165187,
        name: 'Station Penarth Bay',
        location: 'Cardiff',
        sensors: [
            {
                id: 525134,
                type: 'Wind',
                unit: 'km/h',
                data: [
                    { id: 7091, value: 113, datetime: 1708078860717 },
                    { id: 8382, value: 85, datetime: 1708078860717 },
                    { id: 2636, value: 90, datetime: 1708078860717 },
                    { id: 1084, value: 110, datetime: 1708078860717 },
                    { id: 2762, value: 110, datetime: 1708078860717 }
                ]
            },
            {
                id: 878134,
                type: 'Temperature',
                unit: 'Celsius',
                data: [
                    { id: 3378, value: 31, datetime: 1708078860717 },
                    { id: 6379, value: -5, datetime: 1708078860717 },
                    { id: 8348, value: 15, datetime: 1708078860717 },
                    { id: 2223, value: 22, datetime: 1708078860717 },
                    { id: 6437, value: 8, datetime: 1708078860717 }
                ]
            },
            {
                id: 98469355,
                type: 'Humidity',
                unit: '%',
                data: [
                    { id: 7265, value: 91, datetime: 1708078860717 },
                    { id: 5386, value: 53, datetime: 1708078860717 },
                    { id: 4070, value: 0, datetime: 1708078860717 },
                    { id: 7751, value: 12, datetime: 1708078860717 },
                    { id: 8573, value: 46, datetime: 1708078860717 }
                ]
            }
        ]
    }
];

// Routes for the API
app.get('/api/station', (req, res) => {
    res.json(stations);
});

app.get('/api/station/:stationId', (req, res) => {
    const station = stations.find(s => s.id === parseInt(req.params.stationId));
    station ? res.json(station) : res.status(404).send('Station not found');
});

app.get('/api/station/:stationId/sensor', (req, res) => {
    const station = stations.find(s => s.id === parseInt(req.params.stationId));
    station ? res.json(station.sensors) : res.status(404).send('Station not found');
});

app.get('/api/station/:stationId/sensor/:sensorId', (req, res) => {
    const station = stations.find(s => s.id === parseInt(req.params.stationId));
    if (!station) {
        res.status(404).send('Station not found');
        return;
    }
    const sensor = station.sensors.find(s => s.id === parseInt(req.params.sensorId));
    if (sensor) {
        const { data, ...sensorDetails } = sensor;
        res.json(sensorDetails);
    } else {
        res.status(404).send('Sensor not found');
    }
});

app.get('/api/station/:stationId/sensor/:sensorId/data', (req, res) => {
    const station = stations.find(s => s.id === parseInt(req.params.stationId));
    if (!station) {
        res.status(404).send('Station not found');
        return;
    }
    const sensor = station.sensors.find(s => s.id === parseInt(req.params.sensorId));
    sensor ? res.json(sensor.data) : res.status(404).send('Sensor not found');
});

app.get('/api/station/:stationId/sensor/:sensorId/data/:dataId', (req, res) => {
    const station = stations.find(s => s.id === parseInt(req.params.stationId));
    if (!station) {
        res.status(404).send('Station not found');
        return;
    }
    const sensor = station.sensors.find(s => s.id === parseInt(req.params.sensorId));
    if (!sensor) {
        res.status(404).send('Sensor not found');
        return;
    }
    const data = sensor.data.find(d => d.id === parseInt(req.params.dataId));
    data ? res.json(data) : res.status(404).send('Data not found');
});

app.post('/api/station', (req, res) => {
    const { name, location} = req.body;

    if (!name || !location ) {
        res.status(400).send('Invalid station format');
        return;
    }

    // Generate a fake ID for the new station
    const fakeId = Math.floor(Math.random() * 10000);

    // Respond with a success message and the new station's ID
    res.status(201).json({ message: 'Station created', id: fakeId });
});

app.put('/api/station/:stationId', (req, res) => {
    const { stationId } = req.params;
    const { name, location } = req.body;

    const stationIndex = stations.findIndex(s => s.id === parseInt(stationId));
    if (stationIndex === -1) {
        res.status(404).send('Station not found');
        return;
    }

    if (!name || !location ) {
        res.status(400).send('Invalid station format');
        return;
    }

    // To use this route, you would normally update the station in the database
    // stations[stationIndex] = { id: parseInt(stationId), name, location, sensors }; 

    res.json({ message: 'Station updated successfully' });
});

app.post('/api/station/:stationId/sensor/:sensorId/data', (req, res) => {
    const { stationId, sensorId } = req.params;
    const { value, datetime } = req.body;

    const station = stations.find(s => s.id === parseInt(stationId));
    if (!station) {
        res.status(404).send('Station not found');
        return;
    }

    const sensor = station.sensors.find(s => s.id === parseInt(sensorId));
    if (!sensor) {
        res.status(404).send('Sensor not found');
        return;
    }

    if (value === undefined || !datetime) {
        res.status(400).send('Invalid data format');
        return;
    }

    // Generate a fake ID for the new data
    const fakeDataId = Math.floor(Math.random() * 10000);

    // Usualy you would add the new data to the sensor here
    // To simulate this, we'll just respond with a success message and the new data's ID
    res.status(201).json({ message: 'Data added to sensor', dataId: fakeDataId });
});

app.delete('/api/station/:stationId', (req, res) => {
    const { stationId } = req.params;

    const stationIndex = stations.findIndex(s => s.id === parseInt(stationId));
    if (stationIndex === -1) {
        res.status(404).send('Station not found');
        return;
    }

    res.json({ message: 'Station deleted successfully' });
});


// Swagger Documentation
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.listen(port, () => {
    console.log(`Server is running on port ${port}`);
});
