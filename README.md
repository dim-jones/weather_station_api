# A simple station API 

The goal is to create a simple API that will return fake data for stations, sensors and measurements.
It's possible to test the API by GET, POST, PUT and DELETE requests.

## Based on 
The API is based on a simple node.js in version 18. It uses the following packages:
- express
- swagger-ui-express

## How to launch the API in docker 
The API can be launched in a docker container. It has a log file that could be used to debug the API and mounted by a volume. 
```
docker run -d -p 3000:3000 -v weatherapi_logs:/usr/src/app/logs --name weatherapi dimjones/weatherapi 
```
After at least one request, the log file will be created in the volume and you can access it with the following command:
```
docker run -it --rm -v weatherapi_logs:/tmplogs alpine tail -f /tmplogs/activity.log
```

## How to use the API
### A token 
The API need a token to be used. Only the key 'X-Student-Token' is accepted. You can use any value you want.
```
headers: {
        'X-Student-Token': 'what_you_want'
    }
```

### Swagger documentation
The API has a swagger documentation that can be accessed at the following URL: http://localhost:3000/api-docs (replace localhost by your specific IP).

Currently, the API has endpoints and sub-endpoints that can be accessed with the following URLs:
- GET /api/station
- POST /api/station
- GET /api/station/{id}
- PUT /api/station/{id}
- DELETE /api/station/{id}

- GET /api/station/{id}/sensor
- POST /api/station/{id}/sensor/{sensorId}/data

## License
[MIT](https://choosealicense.com/licenses/mit/)



